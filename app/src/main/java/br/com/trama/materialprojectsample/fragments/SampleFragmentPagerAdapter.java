package br.com.trama.materialprojectsample.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.trama.materialprojectsample.R;

public class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    private String tabTitles[] = new String[]{"Table1", "Table2", "Table3", "Table4", "Table5"};
    public static int[] imageResId = {
            R.drawable.ic_action_mail,
            R.drawable.ic_action_shop,
            R.drawable.ic_action_travel,
            R.drawable.icon_nav_shape,
            R.drawable.ic_action_event
    };

    private Context context;

    public SampleFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(position + 1);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}