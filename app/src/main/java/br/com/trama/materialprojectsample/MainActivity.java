package br.com.trama.materialprojectsample;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import br.com.trama.materialprojectsample.fragments.FormFragment;
import br.com.trama.materialprojectsample.fragments.TabFragment;

public class MainActivity extends AppCompatActivity {

    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = configToolbar();

        configDrawer(toolbar);
    }

    private void configDrawer(Toolbar toolBar) {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);

        NavigationViewListener navigationViewListener = new NavigationViewListener();

        navigationView.setNavigationItemSelectedListener(navigationViewListener);

        drawerToggle = new MyActionBarDrawerToggle(toolBar);

        drawerLayout.setDrawerListener(drawerToggle);

        navigationViewListener.onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_tabs));
        navigationView.setCheckedItem(R.id.nav_tabs);
    }
    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private Toolbar configToolbar() {
         toolbar = (Toolbar) findViewById(R.id.toolbar_id);

        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return toolbar;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class NavigationViewListener implements NavigationView.OnNavigationItemSelectedListener {

        @Override
        public boolean onNavigationItemSelected(MenuItem menuItem) {

            boolean isOk = true;

            Fragment fragment = null;
            int rIdTitle = 0;

            switch (menuItem.getItemId()) {
                case R.id.nav_tabs:
                    fragment = new TabFragment();
                    rIdTitle = R.string.tabs;
                    break;
                case R.id.inputs:
                    fragment = new FormFragment();
                    rIdTitle = R.string.input_fab;
                    break;

                default:
                    isOk = false;
            }

            if(isOk) {
                getSupportActionBar().setTitle(rIdTitle);
                openFragment(fragment);
            }

            menuItem.setChecked(menuItem.isChecked());

            drawerLayout.closeDrawers();

            return isOk;
        }
    }

    private void openFragment(Fragment fragment) {

        FragmentManager supportFragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = supportFragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.frame_container_id, fragment);

        fragmentTransaction.commit();
    }

    /**s
     * hamburguer icon
     */
    private class MyActionBarDrawerToggle extends ActionBarDrawerToggle {

        public MyActionBarDrawerToggle(Toolbar toolBar) {
            super(MainActivity.this, MainActivity.this.drawerLayout, toolBar, R.string.hello_world, R.string.hello_world);
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
        }
    }
}
