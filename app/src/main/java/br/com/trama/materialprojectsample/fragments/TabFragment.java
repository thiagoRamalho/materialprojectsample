package br.com.trama.materialprojectsample.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.trama.materialprojectsample.R;

/**
 * Created by thiago on 08/11/15.
 */
public class TabFragment extends android.support.v4.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_layout, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
       super.onViewCreated(view, savedInstanceState);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.view_pager);

        PagerAdapter adapter = new SampleFragmentPagerAdapter(getActivity().getSupportFragmentManager(), getActivity());

        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);

        int tabCount = tabLayout.getTabCount();

        for (int i = 0; i < tabCount; i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setIcon(SampleFragmentPagerAdapter.imageResId[i]);
        }
   }

    /*private ActionBar getActionBar() {
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        return activity.getSupportActionBar();
    }

    private TabLayout.Tab createTab(int rIdIcon) {
        TabLayout.Tab tab = tabLayout.newTab();
        tab.setText(getString(R.string.header_title));
        tab.setIcon(rIdIcon);
        return tab;
    }*/

   /* @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        setElevationToolbar(0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        setElevationToolbar(getResources().getDimension(R.dimen.default_elevation));
    }

    private void setElevationToolbar(float value) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setElevation(value);
            }
        }
    }*/
}
